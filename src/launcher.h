#pragma once

//! this shouldnt depend on OS
#define LIB_CODE_RW_BASE_OFFSET						0xC1000000
#define CODE_RW_BASE_OFFSET							0xC0000000
#define DATA_RW_BASE_OFFSET							0xC0000000

#define ADDRESS_OSTitle_main_entry_ptr				0x1005E040
#define ADDRESS_main_entry_hook						0x0101c56c

#define address_LiWaitIopComplete					0x01010180
#define address_LiWaitIopCompleteWithInterrupts		0x0101006C
#define address_LiWaitOneChunk						0x0100080C
#define address_PrepareTitle_hook					0xFFF184E4
#define address_sgIsLoadingBuffer					0xEFE19E80
#define address_gDynloadInitialized					0xEFE13DBC

#define ROOTRPX_DBAT0U_VAL							0xC00003FF
#define COREINIT_DBAT0U_VAL							0xC20001FF
#define ROOTRPX_DBAT0L_VAL							0x30000012
#define COREINIT_DBAT0L_VAL							0x32000012

///////////////
// Functions //
///////////////

/* Thread callback */
static void thread_callback(int argc, void *argv);

/* Find a ROP gadget by a sequence of bytes */
void *find_gadget(uint32_t code[], uint32_t length, uint32_t gadgets_start);

/* libc functions */
int memcmp(const void *s1p, const void *s2p, size_t n);

//////////////
// Syscalls //
//////////////

/* Arbitrary read and write syscalls */
void __attribute__ ((noinline)) kern_write(void *addr, uint32_t value);
void __attribute__ ((noinline)) KernelCopyData(unsigned int addr, unsigned int src, unsigned int len);

/* assembly functions */
extern void SC_0x25_KernelCopyData(void* addr, void* src, unsigned int len);
extern void Syscall_0x36(void);
extern void KernelPatches(void);




