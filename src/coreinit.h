#pragma once
#include "types.h"

/* ioctlv() I/O vector */
struct iovec
{
	void *buffer;
	int len;
	char unknown8[0xc-0x8];
};

typedef struct OSContext
{
	/* OSContext identifier */
    uint32_t tag1;
    uint32_t tag2;

    /* GPRs */
    uint32_t gpr[32];

	/* Special registers */
    uint32_t cr;
    uint32_t lr;
    uint32_t ctr;
    uint32_t xer;

    /* Initial PC and MSR */
    uint32_t srr0;
    uint32_t srr1;
} OSContext;

/* Dynload functions */
extern int OSDynLoad_Acquire(const char* name, unsigned int *outModule);
extern int OSDynLoad_FindExport(unsigned int module, int isData, const char *name, void *outAddr);
extern void OSDynLoad_Release(unsigned int module);

/* String functions */
extern void *memcpy(void *dst, const void *src, int bytes);
extern void *memset(void *dst, int val, int bytes);
extern int __os_snprintf(char *s, size_t n, const char *format, ...);

/* Memory functions */
extern void* OSAllocFromSystem(uint32_t size, int align);
extern void OSFreeToSystem(void *ptr);

/* Cache functions */
extern void DCFlushRange(void *addr, uint32_t size);
extern void ICInvalidateRange(void *addr, uint32_t size);

/* IM functions */
extern int IM_Open(void);
extern int IM_Close(int fd);
extern int IM_SetDeviceState(int fd, void *mem, int state, int a, int b);

/* Thread functions */	
extern bool OSCreateThread(void *thread, void *entry, int argc, void *args, uint32_t *stack, uint32_t stack_size, int priority, uint16_t attr);
extern int OSResumeThread(void *thread);
extern int OSIsThreadTerminated(void *thread);
extern void OSExitThread(void);

/* OSDriver functions */
extern uint32_t Driver_Register(char *driver_name, uint32_t name_length, void *buf1, void *buf2);
extern uint32_t Driver_CopyToSaveArea(char *driver_name, uint32_t name_length, void *buffer, uint32_t length);

/* Debug functions */
extern void OSFatal(const char *msg);
extern int OSGetSystemTick();

/* Exit functions */
extern void __attribute__((noreturn)) _Exit(int code);
