#pragma once

/* Kernel addresses, stolen from Chadderz */
#define KERN_HEAP				0xFF200000
#define KERN_HEAP_PHYS			0x1B800000

#define KERN_CODE_READ			0xFFF023D4
#define KERN_CODE_WRITE			0xFFF023F4
#define KERN_DRVPTR				0xFFEAB530
#define KERN_ADDRESS_TBL		((uint32_t*)0xFFEAB7A0)

/* Kernel syscall tables */
#define KERN_SYSCALL_TBL_1		0xFFE84C70 // unknown
#define KERN_SYSCALL_TBL_2		0xFFE85070 // works with games
#define KERN_SYSCALL_TBL_3		0xFFE85470 // works with loader
#define KERN_SYSCALL_TBL_4		0xFFEAAA60 // works with home menu
#define KERN_SYSCALL_TBL_5		0xFFEAAE60 // works with browser (previously KERN_SYSCALL_TBL)

/* Kernel heap constants */
#define STARTID_OFFSET			0x08
#define METADATA_OFFSET			0x14
#define METADATA_SIZE			0x10