import struct
import os
import sys


j = 0;

payload = ""
try:
    f = open(os.path.dirname(os.path.abspath(__file__)) + "/wiiuhaxx_loader.bin", "rb")
    while True:
        B = struct.unpack(">B", f.read(1))[0];
        payload += "0x%02x, " % (B)
        j+=1
except:
    payload += "\n"

for i in range(j&0x03):
    payload += "0x00, "
payload += "\n"

payload += "0x00, 0x80, 0x00, 0x00,\n"
j+=4

try:
    f = open(sys.argv[1], "rb")
    while True:
        B = struct.unpack(">B", f.read(1))[0];
        payload += "0x%02x, " % (B)
        j+=1
except:
    payload += ""
    
for i in range(j&0x03):
    payload += "0x00,"
payload += "\n"

#print "["
#print payload
#print "]"

template = open(os.path.dirname(os.path.abspath(__file__)) + "/exploit_template.html", "r")
contents = template.read().replace("$$REPLACEME$$", payload)
template.close()

out = open(sys.argv[2], "w")
out.write(contents)
out.truncate()
out.close()
