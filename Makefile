# Makefile vars
PATH := $(DEVKITPPC)/bin:$(PATH)
SOURCE	:= src
BUILD	:= bin
HTML	:= html
TOOLS	:= tools

# Compiler options
PREFIX	:= powerpc-eabi-
CC		:= $(PREFIX)gcc
AS		:= $(PREFIX)as
LD		:= $(PREFIX)ld
PYTHON	:= python

# Compile flags
CFLAGS	:= -nostdinc -fno-builtin -fno-toplevel-reorder -std=gnu99 -O0
ASFLAGS	:= -nostdinc -fno-builtin -fno-toplevel-reorder -mregnames
LDFLAGS	:= 

# Build options
TARGET	:= $(BUILD)/payload.bin

all: clean setup $(TARGET)
	
setup:
	mkdir -p $(BUILD)
	mkdir -p $(HTML)

$(TARGET):
	$(CC) $(CFLAGS) -c $(SOURCE)/launcher.c -o $(BUILD)/launcher.o
	$(CC) $(ASFLAGS) -c $(SOURCE)/kernel_patches.S -o $(BUILD)/kernel_patches.o
	$(CC) $(ASFLAGS) -c $(SOURCE)/crt0.S -o $(BUILD)/crt0.o
	$(LD) $(LDFLAGS) -T $(SOURCE)/link.ld -o $(BUILD)/payload.bin $(BUILD)/crt0.o $(BUILD)/launcher.o $(BUILD)/kernel_patches.o $(DEVKITPPC)/lib/gcc/powerpc-eabi/6.3.0/libgcc.a
	$(PYTHON) $(TOOLS)/bin2html.py $(TARGET) $(HTML)/payload.html

clean:
	rm -rf $(BUILD)
	rm -rf $(HTML)/payload.html
